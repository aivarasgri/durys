<?php // include "connection.php"; ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Išsirinkite</title>
	<link rel="shortcut icon" href="#"/>
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>


	<div id="price1-div"></div>	
	<div id="price2-div"></div>	
	<div id="content"></div>
	


<!--FIRST STEP-->

<div class="dimension-wrapper" id="form">
	<h1>IVESKINE ANGOS MATMENYS!</h1>
	<form class="dimentions-form" method="POST" action="">
	  <div class="form-group">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p>Aukštis</p>
		<input id="heigth-id" class="form-control" type="number" name="heigth" min="0" max="5000" step="1" value="2500">
		<p>mm</p>
	  </div>
	  <div class="form-group">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p>Plotis</p>
		<input id="width-id" class="form-control" type="number" name="width" min="0" max="5000" step="1" value="1500">
		<p>mm</p>
	  </div>
	  <div class="form-group">
		<p>Durų Kiekis</p>
		<input id="amount-id" class="form-control" type="number" name="width" min="0" max="10" step="1" value="1">
		<p>vnt</p>
	  </div>
	  
	  
	 
	  <img data-string="<?php echo "Stumdomos durys spintai ar nišai"; ?>" src="images/durys_spintai.png" alt="" id="doors-id" onclick="selectValue(this.id)">
	
	
	  <script>
			  	  
		 var imageValue = '';
		  function selectValue(value) {
			imageValue = $('#'+value).attr("data-string");
			imageValue1 = '';
			console.log(imageValue);	  
		 		  
		  }
	  </script>
			

	  
	  <img data-string2="<?php echo "Stumdomos durys pertvarai tarp kambarių"; ?>" src="images/durys_tarp_kabariu.png" alt="" id="doors-id1" onclick="selectValue1(this.id)">
	  
		 <script>
		 var imageValue1 = '';
		  function selectValue1(value1) {
			imageValue1 = $('#'+value1).attr("data-string2");
			imageValue = '';
			console.log(imageValue1);	  
		  }

	   </script>  
	  
	  
	  
	  
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>
	  <input type="" name="" value="NEXT" class="btn btn-primary" id="dimention-btn"><br><br>

	  <br>
	  <br>
	  
  	 <input type="button" name="ajax" value="AJAX" class="btn btn-primary" id="ajaxbutton"><br><br>
  	 
	
		
	  	 
   </form>
</div>

  
<!--SECOND STEP-->

<div class="img-wrapper" id="design-img" class="door-img">
	<img src="images/durys1.png" alt="durys1" id="first-door-design" data-image="images/doorsOneFrame.png"onclick="setImages(this.id); priceCounter1(this.id)">
	<img src="images/durys2.png" alt="durys2" id="second-door-design" data-image="images/doorsTwoFrame.png" data-price2="117" onclick="setImages(this.id); priceCounter1(this.id)">
	<br>
	<p style="display: inline">Dviejų dudų kaina 94</p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="display: inline">Dviejų dudų kaina 117 </p>
	<br>
	<input type="" name="" value="BACK" class="btn btn-primary" id="back-img-btn">
	<input type="" name="" value="NEXT" class="btn btn-primary" id="next-img-btn">
</div>

	<script>
		doorPrice1 = "";
		function priceCounter1(price1) {
			doorPrice1 = price1;
			console.log(doorPrice1);
		}
	</script>

<!--THIRD STEP-->

<!--<form method="post">-->
	<div class="frame-wrapper frame-parent" id="frame" class="frame-img">
	<!--		FIRST FRAME PICKER-->
		<img src="images/medz.jpg"  id="fchild" alt="" data-image="images/doorsOneFrame.png" onclick="setFrame(this.id); priceCounter2(this.id)">
		<img src="images/orion.jpg" id="schild" alt="" onclick="setFrame(this.id); priceCounter2(this.id)">
		<img src="images/orion.jpg" id="thchild" alt="" onclick="setFrame(this.id); priceCounter2(this.id)">
		<img src="images/orion.jpg" id="frchild" alt="" onclick="setFrame(this.id); priceCounter2(this.id)">
		<br>
		<p style="display: inline">Dviejų dudų kaina 94</p>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<p style="display: inline">Dviejų dudų kaina 103 </p>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<p style="display: inline">Dviejų dudų kaina 128 </p>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<p style="display: inline">Dviejų dudų kaina 141 </p>
		<br>
			<div id="fchilds-chil-container">
				<img src="images/pl_geltona.jpg" id="fchilds-child1" alt="" onclick="setFrameColor(this.id); canvas(255, 0, 0);">
				<img src="images/pl_geltona.jpg" id="fchilds-child2" alt="" onclick="setFrameColor(this.id); canvas(51, 204, 51);">
			</div>
			<div id="schilds-chil-container">
				<img src="images/pl_geltona.jpg" id="schilds-child1" alt="" onclick="setFrameColor(this.id); canvas(51, 51, 204);">
				<img src="images/pl_geltona.jpg" id="schilds-child2" alt="" onclick="setFrameColor(this.id); canvas(255, 255, 0);">
				<img src="images/pl_geltona.jpg" id="schilds-child2" alt="" onclick="setFrameColor(this.id); canvas(51, 204, 51);">
			</div>		
	<!--		SECOND FRAME PICKER-->
		<input type="" name="" value="BACK" class="btn btn-primary" id="back-frame-btn">
		<input type="" name="" value="NEXT" class="btn btn-primary" id="next-frame-btn">
	</div>
	
	
	<script>
		doorPrice2 = "";
		function priceCounter2(price2) {
			doorPrice2 = price2;
			console.log(doorPrice2);
		}
	</script>
	
<!--
	<script>
		doorPrice2 = "";
			function priceCounter1(price1) {
					doorPrice1 = price1;
					console.log(doorPrice1);
				}
	</script>
-->
<!--</form>-->
	

<!--FORTH STEP-->
	 
<div id="door-design-and-color" class="door-design-and-color-wrapper">
	<img id="door-design-and-color-picer-img" src="" alt="">
	<div id="one-frame-colors">
		<img src="images/color3.jpg" alt="" id="mainImage" class="door-color">
	</div>	
	<div id="two-frame-colors">
		<img src="images/color3.jpg" alt="" id="mainImageTwoframe1" class="door-color-first">
		<img src="images/color3.jpg" alt="" id="mainImageTwoframe2" class="door-color-second">
	</div>	
	<br>	
	<div id="one-frame-color-picker-btn">
		<button onclick="changeImageColorleft()" class="btn btn-primary">Switch color left</button>
		<button onclick="changeImageColor()" class="btn btn-primary">Switch color right</button>
	</div>
	<br><br>	
	<div id="second-color-buttons">
		<button onclick="changeImageColorleft2Frame()" class="btn btn-primary">Switch color left</button>
		<button onclick="changeImageColorRight2Frame()" class="btn btn-primary">Switch color right</button>
		<br><br>
		<button onclick="changeImageColorleft2FrameSecond()" class="btn btn-primary">Switch color left</button>
		<button onclick="changeImageColorRight2FrameSecond()" class="btn btn-primary">Switch color right</button>
	</div>
	<br>
	<input type="" name="" value="BACK" class="btn btn-primary" id="back-color-btn">
	<input type="" name="" value="NEXT" class="btn btn-primary" id="next-color-btn">
</div>	

<!--FIFTH STEP-->


<div id="final-design-result">

 <img id="final-design-oneframe" class="final-design-oneframe" src="" alt="">
 
	 <div id="final-design-oneframe-color-div">
		<img src="" alt="" id="final-design-oneframe-color" class="final-step-door-color">
	 </div>

	 <div id="final-design-twoframe-color-div">
		 <img src="" alt="" id="final-design-twoframe-color1" class="final-step-door-color-first">
		 <img src="" alt="" id="final-design-twoframe-color2" class="final-step-door-color-second">
	 </div>
	
	 
 
	<br>
	<input type="" value="BACK" class="btn btn-primary" id="back-finaldesign-btn">
	
</div>
	



	
<!--BACK END BELOW	-->
<?php
//
//$user=$_POST['heigth'];
//
//
//echo "username=".$user;

?>
	
<?php
	
//if(isset($_POST['next'])){
//
//	$heigth = $_POST['heigth'];
//	$width = $_POST['heigth'];
//	
//	echo $heigth . "<br>";
//	echo $width;
//    
//    $insertQuery = "INSERT INTO duru_matmenys (door_heigth, door_width) VALUES('$heigth', '$width')";
//    $result = mysqli_query($connection, $insertQuery);
//    
//    if(!$result){
//        die("Insert Failed" . mysqli_error($connection));
//    } else {
//        
//        echo "<h1>Data Added Successfully!</h1>";  
//     
//    }
//}

?>
	 

<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="scripts/script.js" ></script>	
<script src="scripts/ajax.js"></script>	
</body>
</html>